from application import app

# Use individual models as:
# s = models.Student()
# assuming Student is a class in studentModel.py
from application.models import *

from application.config import *
from application.logic.validation import require_role

from flask import \
    render_template, \
    request, \
    url_for

# PURPOSE: Shows all the courses in a major.
@app.route('/stu/showMajor/<string:maj>', methods = ['GET'])
@require_role('students')
def showMajor(maj):
  return render_template("views/stu/showMajorView.html", config = config)


# PURPOSE: Show by required year.
@app.route('/stu/byYear', methods = ['GET'])
@require_role('admin')
def showByYear():
  return render_template("views/stu/showByYearView.html", config = config)


# PURPOSE: Show by hair color.
@app.route('/stu/byHairColor/<string:color>', methods = ['GET'])
@require_role('students')
def showByHairColor(color):
  return render_template("views/stu/showByHairColorView.html", config = config)

